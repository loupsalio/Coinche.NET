﻿using NetworkCommsDotNet.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coinche
{
    public class Player
    {
        List<Card> _hand = new List<Card>();
        String _name;
        Connection _connection;

        public Player(String name_)
        {
            _name = name_;
        }

        public Player(String name_, Connection connection_)
        {
            _name = name_;
            _connection = connection_;
        }

        public int get_nb_cards()
        {
            return _hand.Count;
        }
        
        public Card get_card(int i)
        {
            Card tmp = _hand.ElementAt(i);
            _hand.RemoveAt(i);

            return tmp;
        }

        public void give_card(Card card)
        {
            _hand.Add(card);
        }

        public String get_name()
        {
            return _name;
        }
        
        public void send(String msgType, String msg)
        {
            _connection.SendObject(msgType, msg);
        }

    }

}
