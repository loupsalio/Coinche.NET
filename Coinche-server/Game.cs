﻿using Coinche;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Coinche
{
    public class Game
    {
        int _nb_players = 0;
        List<Player> _players = new List<Player>();
        List<Card> _tas = new List<Card>();
        Deck _deck = new Deck();
        int _score1 = 0;
        int _score2 = 0;
        int _play = 0;
        int _check = 0;

        public int   add_player(Player player)
        {
            if (_nb_players >= 2)
                return 1;
            _players.Add(player);
            _nb_players++;
            return 0;
        }

        public int round()
        {
            _play = 1;
            _check = 0;
            _players.ElementAt(0).send("message", "It's your turn, press enter");
            while (_check != 1)
            {
                Thread.Sleep(200);
            };
            _tas.Add(_players.ElementAt(0).get_card(0));
            broadcast(_players.ElementAt(0).get_name() + " : " + _tas.ElementAt(_tas.Count - 1).toString());
            _play = 2;
            _check = 0;
            _players.ElementAt(1).send("message", "It's your turn, press enter");
            while (_check != 1)
            {
                Thread.Sleep(200);
            };
            _tas.Add(_players.ElementAt(1).get_card(0));
            broadcast(_players.ElementAt(1).get_name() + " : " + _tas.ElementAt(_tas.Count - 1).toString());
            _check = 0;
            _play = 0;

            if (_tas.ElementAt(_tas.Count - 2).get_value() >
                _tas.ElementAt(_tas.Count - 1).get_value())
            {
                broadcast(_players.ElementAt(0).get_name() + " get it !");
                for (int a = 0; a < _tas.Count; a++)
                    _score1 += _tas.ElementAt(a).get_value();
                _tas.Clear();
            }
            else if (_tas.ElementAt(_tas.Count - 2).get_value() <
                _tas.ElementAt(_tas.Count - 1).get_value())
            {
                broadcast(_players.ElementAt(1).get_name() + " get it !");
                for (int a = 0; a < _tas.Count; a++)
                    _score2 += _tas.ElementAt(a).get_value();
                _tas.Clear();
            }
            else
            {
                if (_players.ElementAt(0).get_nb_cards() != 0 &&
_players.ElementAt(1).get_nb_cards() != 0)
                {
                    _tas.Add(_players.ElementAt(0).get_card(0));
                    _tas.Add(_players.ElementAt(1).get_card(0));
                    broadcast("Bataille !");
                    round();
                }
                else
                {
                    broadcast("It's a draw !");
                    return 1;
                }
            }
            return 0;
        }

        private void results()
        {
            if (_score1 > _score2)
                broadcast("Player " + _players.ElementAt(0).get_name() + " Won");
            else if (_score2 > _score1)
                broadcast("Player " + _players.ElementAt(1).get_name() + " Won");
            else
                broadcast("It's a draw !");
        }

        private void begin()
        {
            broadcast("Game is ready!");
            broadcast("------------");
            broadcast("Players : ");
            broadcast("- " + _players.ElementAt(0).get_name());
            broadcast("- " + _players.ElementAt(1).get_name());
            broadcast("------------");
            broadcast("Game start!");
        }

        public int run()
        {
            if (_nb_players != 2)
                return 1;
            begin();
            cards_distibute();
            while (_players.ElementAt(0).get_nb_cards() != 0 &&
                _players.ElementAt(1).get_nb_cards() != 0)
            {
                if (round() != 0)
                    break;
            }
            results();
            return 0;
        }

        public void cards_distibute()
        {
            int i = 0;
            while (_deck.get_size() != 0) {
                {
                    _players.ElementAt(i).give_card(_deck.get_card());
                    i++;
                    if (i >= _players.Count)
                        i = 0;
                }
            }
        }

        void    broadcast(String msg)
        {
            _players.ElementAt(0).send("message", msg);
            _players.ElementAt(1).send("message", msg);
#if DEBUG
            System.Console.WriteLine(msg);
#endif
        }

        public void check(int i)
        {
            if (i == _play)
                _check = 1;
        }

    }
}
