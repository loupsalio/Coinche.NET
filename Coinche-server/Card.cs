﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coinche
{
    public enum color
    {
        pique,
        trefle,
        carreau,
        coeur
    }

    public class Card
    {
        int     _value;
        color   _color;

        public Card(int value_, color color_)
        {
            _value = value_;
            _color = color_;
        }

        public int get_value()
        {
            return _value;
        }

        public String better_value()
        {
            if (_value == 11)
                return "Valet";
            else if (_value == 12)
                return "Dame";
            else if (_value == 13)
                return "Roi";
            else if (_value == 14)
                return "As";
            else
            return _value.ToString();
        }

        public color get_color()
        {
            return _color;
        }

        public String toString()
        {
            return better_value() + " de " + _color;
        }
    }
}
