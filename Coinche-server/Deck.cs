﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coinche
{
    public class Deck
    {

        List<Card> _cards = new List<Card>();

        public Deck()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int o = 7; o < 15; o++)
                {
                    _cards.Add(new Card(o, (color)i));
                }
            }
            this.shuffle();
        }

        public void shuffle()
        {
            var random = new System.Random();
            _cards.Sort((x, y) => random.Next(-1, 2));
        }

        public void show_deck()
        {
            for(int i = 0; i < _cards.Count;i++)
            {
                _cards.ElementAt(i).ToString();
            }
        }

        public int get_size()
        {
            return _cards.Count;
        }

        public  Card get_card()
        {
            if (get_size() < 1)
                return null;
            Card tmp = _cards.ElementAt(0);
            _cards.RemoveAt(0);
            return tmp;
        }

    }
}
