﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coinche
{
    class Server
    {
        Game my_game = new Game();
        Player p1 = null;
        Player p2 = null;

        public void launch()
        {
            String tmp = "";

            NetworkComms.AppendGlobalIncomingPacketHandler<string>("ok",
                    (packetHeader, connection, msg) =>
                    {
                        if (msg == p1.get_name())
                            my_game.check(1);
                        else
                            my_game.check(2);
                    });
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("new",
                   (packetHeader, connection, name) =>
                   {
                       if (p1 == null)
                       {
                           p1 = new Player(name, connection);
                           connection.SendObject("message", "Waiting another player");
                       }
                       else if (p2 == null)
                       {
                           p2 = new Player(name, connection);
                           my_game.add_player(p1);
                           my_game.add_player(p2);
                           my_game.run();
                           my_game = new Game();
                       }
                       else
                       {
                           connection.SendObject("message", "Server full");
                           connection.CloseConnection(true);
                       }
                   });
            NetworkComms.AppendGlobalConnectionCloseHandler((Connection connection)=>
            {
                NetworkComms.CloseAllConnections();
                my_game = new Game();
                p1 = null;
                p2 = null;
            });
            Connection.StartListening(ConnectionType.TCP, new System.Net.IPEndPoint(System.Net.IPAddress.Any, 4200));

            Console.WriteLine("------------");
            Console.WriteLine("Server ready");
            Console.WriteLine("------------");

            while (tmp != "/close")
            {
                Console.Write("cmd : ");
                tmp = Console.ReadLine();
                if (tmp == "/infos")
                {
                    Console.WriteLine("Server listening for TCP connection on:");
                    foreach (System.Net.IPEndPoint localEndPoint in Connection.ExistingLocalListenEndPoints(ConnectionType.TCP))
                        Console.WriteLine("{0}:{1}", localEndPoint.Address, localEndPoint.Port);
                }
                else if (tmp == "/msg")
                {
                    String msg;
                    Console.WriteLine("Write Message");
                    msg = Console.ReadLine();
                    List<Connection> _users = NetworkComms.GetExistingConnection();
                    for (int i = 0; i < _users.Count(); i++)
                    {
                        _users.ElementAt(i).SendObject("message", msg);
                    }
                }
                else if (tmp == "/players")
                {
                    System.Console.WriteLine("Players : ");
                    if (p1 != null)
                        System.Console.WriteLine("- " + p1.get_name());
                    if (p2 != null)
                        System.Console.WriteLine("- " + p2.get_name());
                }

            }
            NetworkComms.Shutdown();
        }

        private void sendMsg(String msgType, String msg)
        {
            List<Connection> _users = NetworkComms.GetExistingConnection();
            for (int i = 0; i < _users.Count(); i++)
            {
                _users.ElementAt(i).SendObject(msgType, msg);
            }
        }

        
    }
}
