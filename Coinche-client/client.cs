﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.TCP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Coinche
{
    class Client
    {
        private static string Server = "localhost";
        private static int Port = 4200;

        static void Main(string[] args)
        {
            String _name;
            var slaveId = Process.GetCurrentProcess().Id;
            var endpoint = GetIPEndPointFromHostName(Server, Port);
            var connection = TCPConnection.GetConnection(new ConnectionInfo(endpoint));


            NetworkComms.AppendGlobalIncomingPacketHandler<string>("message", PrintIncomingMessage);

            Console.Write("Enter your name :");
            _name = Console.ReadLine();
            connection.SendObject("new", _name);

            while (true)
            {
                Console.ReadLine();
                connection.SendObject("ok", _name);
            }
        }

        private static IPEndPoint GetIPEndPointFromHostName(string hostName, int port)
        {
            var addresses = Dns.GetHostAddresses(hostName);
            if (addresses.Length == 0)
            {
                throw new ArgumentException(
                    "Unable to retrieve address from specified host name.",
                    "hostName"
                    );
            }
            return new IPEndPoint(addresses[0], port);
        }
        private static void PrintIncomingMessage(PacketHeader header, Connection connection, string message)
        {
            Console.WriteLine(message);
        }
    }
}