using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestCoinche
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCard()
        {
            Coinche.Card tmp = new Coinche.Card(9, Coinche.color.trefle);
            Assert.AreEqual(tmp.get_value(), 9);
            Assert.AreEqual(tmp.get_color(), Coinche.color.trefle);
        }

        [TestMethod]
        public void TestDeck()
        {
            Coinche.Deck my_deck = new Coinche.Deck();
            Assert.AreEqual(my_deck.get_size(), 32); 
        }

        [TestMethod]
        public  void TestShuffle()
        {
            Coinche.Deck my_deck = new Coinche.Deck();
            Assert.AreNotEqual(my_deck.get_card(), new Coinche.Card(7, (Coinche.color)0));
        }


        [TestMethod]
        public void TestDistribute()
        {
            Coinche.Game my_game = new Coinche.Game();
            Coinche.Player p1 = new Coinche.Player("jack");
            Coinche.Player p2 = new Coinche.Player("jacko");
 
            my_game.add_player(p1);
            my_game.add_player(p2);

            my_game.cards_distibute();
            
            Assert.AreEqual(p1.get_nb_cards(), 16);
            Assert.AreEqual(p2.get_nb_cards(), 16);
        }

    }
}
